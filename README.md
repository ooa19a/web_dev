# Web page

##

> Here is a webpage I developed using the following technologies

* HTML
* CSS, and
* JavaScript

### Random Notes

####

##### Started with the nav bar. I built a grid system from scratch. Added a form for accepting userdetails. I then proceeded to embed video using iframe. Add button and did some styling to it using CSS

| **Webpage on full screen**  | **Webpage on full screen (...contd)** |
|-------------------------------|-------------------------|
| ![Webpage Image](img/webpage1.png) | ![Webpage Image](img/webpag2.png) |

##### I used inline JavaScript to make the website a little more interactive and I made the website mobile resopnsive

| **Webpage on mbile screen**  | **Webpage on mobile screen** |**Webpage on mobile screen** |**Webpage on mobile screen (...contd)** |
|-------------------------------|-------------------------|----------|-------|
| ![Webpage Image](img/webpage_mobileresponsive.png)  | ![Webpage Image](img/webpage_mobileresponsive4.png)| ![Webpage Image](img/webpage_mobileresponsive2.png)| ![Webpage Image](img/webpage_mobileresponsive3.png)|
